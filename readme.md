[![CircleCI](https://circleci.com/bb/phpwedge/container.svg?style=svg)](https://circleci.com/bb/phpwedge/container)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/938e93926088436fa3124e9518588999)](https://www.codacy.com/app/PhpWedge/container?utm_source=phpwedge@bitbucket.org&amp;utm_medium=referral&amp;utm_content=phpwedge/container&amp;utm_campaign=Badge_Grade)
[![Codacy Badge](https://api.codacy.com/project/badge/Coverage/938e93926088436fa3124e9518588999)](https://www.codacy.com/app/PhpWedge/container?utm_source=phpwedge@bitbucket.org&utm_medium=referral&utm_content=phpwedge/container&utm_campaign=Badge_Coverage)
[![Latest Stable Version](https://poser.pugx.org/phpwedge/container/v/stable)](https://packagist.org/packages/phpwedge/container)
[![Total Downloads](https://poser.pugx.org/phpwedge/container/downloads)](https://packagist.org/packages/phpwedge/container)
[![License](https://poser.pugx.org/phpwedge/container/license)](https://packagist.org/packages/phpwedge/container)
# PhpWedge Container Package
>This package contains a simple psr container implementation. It's also implementing ArrayAccess and JsonSerializable for the easier usage.
### How to setup?
#### via composer cli
````bash
composer require phpwedge/container
````
#### via composer.json
````json
  "require": {
    "phpwedge/container": "^1.0.0"
  }
````
### How to use?

#### set the entries via ````__construct````
````php
<?php
use PhpWedge\Core\Container;

$entries = [
    'a' => 'b',
    'c' => 'd',
];

$container = new Container($entries);
````

#### set the entries via ````setEntries()```` method
````php
<?php
use PhpWedge\Core\Container;

$entries = [
    'a' => 'b',
    'c' => 'd',
];

$container = new Container();
$container->setEntries($entries);
````

#### manipulate entries
````php
<?php
use PhpWedge\Core\Container;

$container = new Container();
$container->set('c', 'd');
$container->set('e', 'f');

if ($container->has('e')) {
    $value = $container->get('e');
    $container->remove('e');
}

// !!!Be careful because if the entry does not exist, it throws an exception!!! 
try {
    $value = $container->get('a');
}
catch (\PhpWedge\Core\OffsetNotFoundException $e) {
    // Do something
}

$entries = $container->getEntries();
````

#### manipulate entries via ArrayAccess
````php
<?php
use PhpWedge\Core\Container;

$container = new Container();
$container['e'] = 'f';

if (isset($container['e'])) {
    $value = $container['e'];
    unset($container['e']);
}
````

#### get the entries in json
````php
<?php
use PhpWedge\Core\Container;

$container = new Container();
$container['e'] = 'f';

$entriesJson = json_encode($container);
````
### Contribution
> If you miss any kind of functionality or you noticed an error, please create an issue!
### References
* [https://github.com/container-interop/fig-standards/blob/master/proposed/container.md](https://github.com/container-interop/fig-standards/blob/master/proposed/container.md)
* [http://php.net/manual/en/class.arrayaccess.php](http://php.net/manual/en/class.arrayaccess.php)
* [http://php.net/manual/en/class.jsonserializable.php](http://php.net/manual/en/class.jsonserializable.php)
