<?php
/**
 * MIT License
 *
 * Copyright (c) 2017 Vilmos "Chilly" Kovacs
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

declare(strict_types=1);

namespace PhpWedgeTest\Core;


use PHPUnit\Framework\TestCase;
use PhpWedge\Core\Container;
use PhpWedge\Core\OffsetNotFoundException;

class ContainerTest extends TestCase
{
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    protected $mockInstance;

    /**
     * Sets up the test mock object for each of the tests.
     */
    public function setUp()
    {
        parent::setUp();

        $this->mockInstance = $this->getMockBuilder(Container::class)->getMockForAbstractClass();
    }

    /**
     * Test for the setEntries and getEntries methods.
     */
    public function testSetterGetter()
    {
        $entriesFixture = ['a' => 'b', 'c' => 'd', 'e' => ['a' => 'b']];

        $this->mockInstance->setEntries($entriesFixture);
        $this->assertEquals($entriesFixture, $this->mockInstance->getEntries());
    }

    /**
     * Test for the has method.
     *
     * @param bool   $expected   The expected outcome.
     * @param string $id         The identifier of the entry.
     * @param array  $entries    The manageable entries.
     *
     * @dataProvider hasProvider
     */
    public function testHas($expected, $id, $entries)
    {
        $this->mockInstance->setEntries($entries);
        $this->assertEquals($expected, $this->mockInstance->has($id));
    }

    /**
     * Data provider for the test of the has method.
     *
     * @return array
     */
    public function hasProvider()
    {
        return [
            [
                true,
                'a',
                ['a' => 'b']
            ],
            [
                true,
                'a',
                ['a' => '']
            ],
            [
                true,
                'a',
                ['a' => false]
            ],
            [
                true,
                'a',
                ['a' => 0]
            ],
            [
                false,
                'b',
                ['a' => 'b']
            ],
        ];
    }

    /**
     * Test of the get method on failure.
     */
    public function testGetOnFailure()
    {
        $entriesFixture = ['a' => 'b'];

        $this->expectException(OffsetNotFoundException::class);
        $this->expectExceptionMessage('The requested offset(baa) does not exist!');

        $this->mockInstance->setEntries($entriesFixture);
        $this->mockInstance->get('baa');
    }

    /**
     * Test the get method on success.
     */
    public function testGetOnSuccess()
    {
        $entriesFixture = ['a' => 'b'];

        $this->mockInstance->setEntries($entriesFixture);
        $this->assertEquals('b', $this->mockInstance->get('a'));
    }

    /**
     * Test the set method.
     */
    public function testSet()
    {
        $offsetFixture = 'offset';
        $valueFixture  = 'aaa';

        $this->mockInstance->set($offsetFixture, $valueFixture);
        $this->assertEquals($valueFixture, $this->mockInstance->get($offsetFixture));
    }

    /**
     * Test the remove method.
     */
    public function testRemove()
    {
        $offsetFixture          = 'offset';
        $originalEntriesFixture = ['a' => 'b', $offsetFixture => 'value', 'c' => 'd'];
        $modifiedEntriesFixture = ['a' => 'b', 'c' => 'd'];

        $this->mockInstance->setEntries($originalEntriesFixture);
        $this->mockInstance->remove($offsetFixture);
        $this->assertEquals($modifiedEntriesFixture, $this->mockInstance->getEntries());
    }

    /**
     * Test the array access offsetExists method.
     */
    public function testOffsetExists()
    {
        $entriesFixture = ['a' => 'b'];

        $this->mockInstance->setEntries($entriesFixture);
        $this->assertTrue(isset($this->mockInstance['a']));
        $this->assertFalse(isset($this->mockInstance['b']));
    }

    /**
     * Test the array access offsetGet method.
     */
    public function testOffsetGet()
    {
        $entriesFixture = ['a' => 'b'];

        $this->mockInstance->setEntries($entriesFixture);
        $this->assertEquals('b', $this->mockInstance['a']);
    }

    /**
     * Test the array access offsetSet method.
     */
    public function testOffsetSet()
    {
        $offsetFixture = 'offset';
        $valueFixture  = 'aaa';

        $this->mockInstance[$offsetFixture] = $valueFixture;
        $this->assertEquals($valueFixture, $this->mockInstance[$offsetFixture]);
    }

    /**
     * Test the array access offsetUnset method.
     */
    public function testOffsetUnSet()
    {
        $offsetFixture = 'offset';

        $this->mockInstance[$offsetFixture] = 'a';
        unset($this->mockInstance[$offsetFixture]);
        $this->assertFalse(isset($this->mockInstance[$offsetFixture]));
    }

    /**
     * Test for the jsonSerialize method.
     */
    public function testJsonSerialize()
    {
        $entriesFixture     = ['a' => 'b', 'c' => 'd', 'e' => ['a' => 'b']];
        $jsonEntriesFixture = '{"a":"b","c":"d","e":{"a":"b"}}';

        $this->mockInstance->setEntries($entriesFixture);
        $this->assertEquals($jsonEntriesFixture, json_encode($this->mockInstance));
    }
}
