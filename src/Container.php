<?php
/**
 * MIT License
 *
 * Copyright (c) 2017 Vilmos "Chilly" Kovacs
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

declare(strict_types=1);

namespace PhpWedge\Core;


use Psr\Container\ContainerInterface;

class Container implements ContainerInterface, \ArrayAccess, \JsonSerializable
{
    /**
     * @var array
     */
    private $entries;

    /**
     * Container constructor.
     *
     * @param array $entries
     */
    public function __construct(array $entries = [])
    {
        $this->setEntries($entries);
    }

    /**
     * Returns the entries.
     *
     * @return array
     */
    public function getEntries(): array
    {
        return $this->entries;
    }

    /**
     * Sets the container.
     *
     * @param array $entries
     *
     * @return void
     */
    public function setEntries(array $entries)
    {
        $this->entries = $entries;
    }

    /**
     * @inheritdoc
     */
    public function get($id)
    {
        if (!$this->has($id)) {
            throw new OffsetNotFoundException('The requested offset(' . $id . ') does not exist!');
        }

        return $this->entries[$id];
    }

    /**
     * @inheritdoc
     */
    public function has($id)
    {
        if (isset($this->entries[$id])) {
            return true;
        }

        return false;
    }

    /**
     * Sets an entry of the container under its identifier.
     *
     * @param string $id      Identifier of the entry to set.
     * @param mixed  $value   Value of the entry to set.
     *
     * @return void
     */
    public function set(string $id, $value)
    {
        $this->entries[$id] = $value;
    }

    /**
     * Removes an entry from the container.
     *
     * @param string $id      Identifier of the entry to remove.
     *
     * @return void
     */
    public function remove(string $id)
    {
        unset($this->entries[$id]);
    }

    /**
     * @inheritdoc
     */
    public function offsetExists($offset)
    {
        return $this->has($offset);
    }

    /**
     * @inheritdoc
     */
    public function offsetGet($offset)
    {
        return $this->get($offset);
    }

    /**
     * @inheritdoc
     */
    public function offsetSet($offset, $value)
    {
        $this->set($offset, $value);
    }

    /**
     * @inheritdoc
     */
    public function offsetUnset($offset)
    {
        $this->remove($offset);
    }

    /**
     * @inheritdoc
     */
    public function jsonSerialize()
    {
        return $this->getEntries();
    }
}
